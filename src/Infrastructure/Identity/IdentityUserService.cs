﻿using KaiShing.Application.Common.Interfaces;
using KaiShing.Application.Common.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KaiShing.Infrastructure.Identity
{
    public class IdentityUserService : IIdentityUserService
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public IdentityUserService(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task<string> GetUserNameAsync(string userId)
        {
            var user = await _userManager.Users.FirstAsync(u => u.Id == userId);
            return user.UserName;
        }
        public async Task<Result> GetUserAsync(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);
            return user == null ? Result.Failure("User does not exist.") :
                Result.Success(new UserDetail()
                {
                    UserName = user.UserName,
                    Name = user.Name,
                    Email = user.Email,
                    PhoneNumber = user.PhoneNumber,
                });
        }

        public async Task<Result> GetUsersAsync()
        {
            var userList = await _userManager.Users.ToListAsync();
            var userDetailList = new List<UserDetail>();
            foreach (var user in userList)
            {
                userDetailList.Add(new UserDetail()
                {
                    UserName = user.UserName,
                    Name = user.Name,
                    Email = user.Email,
                    PhoneNumber = user.PhoneNumber,
                });
            }
            return Result.Success(userDetailList);
        }
        public async Task<Result> CreateUserAsync(string userName, string email, string phoneNumber, string name)
        {
            var user = new ApplicationUser
            {
                UserName = userName,
                Email = email,
                PhoneNumber = phoneNumber,
                Name = name
            };

            var result = await _userManager.CreateAsync(user, "pw" + userName);

            return (result.ToApplicationResult(user.Id));
        }


        public async Task<Result> DeleteUserAsync(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);
            if (user != null)
            {
                var result = await _userManager.DeleteAsync(user);
                return result.ToApplicationResult();
            }

            return Result.Success();
        }

        public async Task<Result> ResetPasswordAsync(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);
            if (user != null)
            {
                var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                var result = await _userManager.ResetPasswordAsync(user, token, "pw" + userName);
                return result.ToApplicationResult();
            }

            return Result.Success();
        }



        public async Task<Result> UpdateUserInfoAsync(string userName, string email, string phoneNumber, string name)
        {
            var user = new ApplicationUser
            {
                UserName = userName,
                Email = email,
                PhoneNumber = phoneNumber,
                Name = name
            };
            var result = await _userManager.UpdateAsync(user);
            return result.ToApplicationResult();
        }


    }
}
