﻿using KaiShing.Application.Common.Interfaces;
using KaiShing.Application.Common.Models;
using KaiShing.Domain.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace KaiShing.Infrastructure.Identity
{
    public class IdentityRoleService : IIdentityRoleService
    {
        private readonly RoleManager<Role> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public IdentityRoleService(RoleManager<Role> roleManager, UserManager<ApplicationUser> userManager)
        {
            _roleManager = roleManager;
            _userManager = userManager;
        }

        public async Task<Result> GetRoleAsync(string roleId)
        {
            var role = await _roleManager.FindByIdAsync(roleId);
            var claims = await _roleManager.GetClaimsAsync(role);
            return role == null ? Result.Failure("Role does not exist.") :
                Result.Success(new RoleDetail()
                {
                    RoleId = role.Id,
                    RoleName = role.Name,
                    RoleDescription = role.RoleDescription,
                    RoleClaim = claims
                });
        }
        public async Task<Result> GetRolesAsync()
        {
            var roleList = await _roleManager.Roles.ToListAsync();
            var roleDetailsList = new List<RoleDetail>();
            foreach (var role in roleList)
            {
                var rd = new RoleDetail()
                {
                    RoleId = role.Id,
                    RoleName = role.Name,
                    RoleDescription = role.RoleDescription,
                };
                rd.RoleClaim = await _roleManager.GetClaimsAsync(role);
                roleDetailsList.Add(rd);

            }

            return Result.Success(roleDetailsList);
        }
        public async Task<Result> CreateRoleAsync(string roleName, List<string> roleClaims, string roleDescription = null)
        {
            var role = new Role
            {
                Name = roleName,
                RoleDescription = roleDescription
            };

            var result = await _roleManager.CreateAsync(role);
            if (!result.Succeeded)
            {
                return (result.ToApplicationResult());
            }
            foreach (var item in roleClaims)
            {
                await _roleManager.AddClaimAsync(role, new Claim("Claim.Permission", item));
            }
            return Result.Success(role.Id);
        }
        public async Task<Result> UpdateRoleAsync(string roleName, List<string> roleClaims, string roleDescription = null)
        {
            var role = new Role
            {
                Name = roleName,
                RoleDescription = roleDescription
            };

            var result = await _roleManager.UpdateAsync(role);
            if (!result.Succeeded)
            {
                return result.ToApplicationResult();
            }
            var oldClaims = await _roleManager.GetClaimsAsync(role);
            foreach (var item in oldClaims)
            {
                await _roleManager.RemoveClaimAsync(role, item);
            }
            foreach (var item in roleClaims)
            {
                await _roleManager.AddClaimAsync(role, new Claim("Claim.Permission", item));
            }
            return Result.Success();
        }
        public async Task<Result> DeleteRoleAsync(string roleId)
        {
            var role = await _roleManager.FindByIdAsync(roleId);
            if (role != null)
            {
                var result = await _roleManager.DeleteAsync(role);
                return result.ToApplicationResult();
            }
            return Result.Success();
        }

        public async Task<Result> AssignAsync(string roleId, string[] usersName)
        {
            var role = await _roleManager.FindByIdAsync(roleId);
            var removeUsers = await _userManager.GetUsersInRoleAsync(role.Name);
            //romove user
            foreach (var item in removeUsers)
            {
                await _userManager.RemoveFromRoleAsync(item, role.Name);
            }
            //assign user
            foreach (var item in usersName)
            {
                var user = await _userManager.FindByNameAsync(item);
                await _userManager.AddToRoleAsync(user, role.Name);
            }
            return Result.Success();
        }
    }
}
