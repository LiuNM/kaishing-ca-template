﻿using KaiShing.Application.Common.Interfaces;
using KaiShing.Application.TodoItems.Commands.CreateTodoItem;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using KaiShing.Domain.Entities;
using KaiShing.Domain.Enums;
using KaiShing.Application.Common.Models;

namespace KaiShing.Application.Roles.Commadns.DeleteRoles
{
    public class DeleteRolesCommand : IRequest<Result>
    {

        public string RoleId { get; set; }
    }

    public class DeleteRolesCommandHandler : IRequestHandler<DeleteRolesCommand, Result>
    {
        
        private readonly IApplicationDbContext _context;
        private readonly IIdentityRoleService _identityRoleService;

        public DeleteRolesCommandHandler(IApplicationDbContext context, IIdentityRoleService identityRoleService)
        {
            _context = context;
            _identityRoleService = identityRoleService;
        }

        public async Task<Result> Handle(DeleteRolesCommand request, CancellationToken cancellationToken)
        {
            return await _identityRoleService.DeleteRoleAsync(request.RoleId);
        }
    }
}
