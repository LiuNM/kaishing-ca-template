﻿using KaiShing.Application.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KaiShing.Application.Common.Interfaces
{
    public interface IIdentityUserService
    {
        Task<string> GetUserNameAsync(string userId);

        Task<Result> GetUserAsync(string userName);

        Task<Result> GetUsersAsync();

        Task<Result> CreateUserAsync(string userName, string email, string phoneNumber, string name);

        Task<Result> ResetPasswordAsync(string userName);

        Task<Result> UpdateUserInfoAsync(string userName, string email, string phoneNumber, string name);

        Task<Result> DeleteUserAsync(string userName);
    }
}
